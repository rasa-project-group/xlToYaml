
import os
import argparse
from typing import List
from numpy.core.numeric import NaN
import pandas as pd 
import yaml 
import numpy as np   
from pandas import DataFrame
import urllib.request
from urllib import error
import codecs

import os
def Read_csv_file(path:str)-> DataFrame:
    exl=pd.read_csv(path) 
    return exl
    
def download_file(Url:str,Storage_path:str):
    try:
       urllib.request.urlretrieve(Url, Storage_path)  
       return True
    except error as ex:
       print(ex.msg)     
       return False
   
   
def  extract_nlu_to_yaml(dataframe:DataFrame,drop_list:list) -> str :
    final_result=""

    data=dataframe.drop(['Bot_utter_name','Bot_utter_result'],axis=1).head()
    _Result="version: '2.0'"+"\nnul:\n"
    for i in range(len(dataframe)): 
        intent_name=dataframe.Intent[i]
        intent_name="- intent"+ " : "+ intent_name+"\n"
        
        Examples=str(dataframe.Examples[i])
        splites_examples=Examples.split(",")
        final_Examples=""
        final_Examples="  examples: | \n"
        for item in  splites_examples:
           final_Examples= final_Examples+ "    - "+ item +"\n"
            
        final_result+=intent_name+final_Examples
    with codecs.open("Results/nlu.txt","w","utf-8") as fulk:
         fulk.write(final_result)
    os.rename("Results/nlu.txt","Results/nlu.yml")         
    return _Result

 
def extract_domain_to_yml(dataframe:DataFrame,drop_list:list):
    data_frame.dropna(inplace=True)
    data=dataframe.drop(['Intent','Examples'],axis=1).head()
    data_name_utter=data["Bot_utter_name"]
    data_utter_message=data["Bot_utter_result"]

    print("ok")
    print(data_name_utter.size)
    print(data_utter_message.size)
    

    domain_load=yaml_load("Downloads/domain.yml")   
    responsess=domain_load["responses"]
 
    for utter_name,message in zip(data_name_utter,data_utter_message):

        if(utter_name!="" and utter_name!="nan" and message!="" and message!="nan" ):
            print("zss")
            print(utter_name)
            print(message)
            responsess[utter_name]=[{'text':f"{message}"}]    
        
    dump_yaml(domain_load,'Results/domain.yml')

def extract_nlu_to_yml(dataframe:DataFrame,list:List):
    """
    extract_nlu_to_yml Extract Nlu file

    [extended_summary]

    Args:
        dataframe (DataFrame): [description]
        list (List): [description]
    """
    print("********************************extract_nlu_to_yml")
    data=dataframe.drop(['Bot_utter_name','Bot_utter_result'],axis=1).head()
    data_Intent=data["Intent"]
    data_Examples=data["Examples"]
    print(data_Intent.size)
    print(data_Examples.size)    
    nlu_load=yaml_load("Downloads/nlu.yml")   
    
    nlu_intents=nlu_load["nlu"]
    for intent,examples in zip(data_Intent,data_Examples):
        examples_splited=str(examples).split(",")
        print(examples_splited)
        print(examples_splited)
        if(intent!="" and examples_splited!=None):
            print("intent")
            nlu_intents.append({"itent":intent,"examples":examples_splited})

    print(nlu_intents)
    dump_yaml(nlu_load,'Results/nlu.yml')
        
        
    
    
def dump_yaml(stream,path:str):
    with open(path,"w") as filedec:
        yaml.dump(stream,filedec)  
        
        
        
def yaml_load(path): 
    with open(path,'r') as filedesc:  
     try:   
      data=yaml.safe_load(filedesc)    
      return data
     except yaml.YAMLError:
        if hasattr( 'problem_mark'):
         print ("E"  ) 
        else:print("dds")      




# parser = argparse.ArgumentParser(description="Ready to parse ")

# parser.add_argument('file_link', action='store', help= 'Grep xl')

# arguments = parser.parse_args()

# xl_link = arguments.file_link
# print(xl_link)
storage_path="Downloads/test.csv"
Result_extract="Results/nlu_csv.yml"
drop_list=['Bot_utter_name','Bot_utter_result']
download_file("https://docs.google.com/spreadsheets/d/e/2PACX-1vT8eZmnJEB5ZQxHXItMfXtYxh4G_UKNhRQOlrZRZD5HxWBMUWPMhgV-5El5Ebu7xwZGmXUIq1QovHtl/pub?output=csv",storage_path)

data_frame=Read_csv_file(storage_path)
Result_stream=extract_nlu_to_yaml(data_frame,drop_list)
data_frame=Read_csv_file(storage_path)
extract_domain_to_yml(data_frame,[])











